# purple-cow-ssl-eater

# Need to Get Around?

1. [Get the Repository over SSH](#get-the-repository-over-ssh)
1. [Build and Run locally](#build-and-run-locally)
1. [Build and Publish to ECR](#build-and-publish-to-ecr)
1. [Build out Infrastructure](#build-out-infrastructure)
1. [Future Updates](#future-updates)
1. [Details of Implementation](#details-of-implementation)

## Get the Repository over SSH

1. Create ssh keypair
    ```
    ssh-keygen -t rsa -m PEM -f "$HOME/.ssh/beep" -C "beep"
    ```

1. Copy the contents of beep.pub and add to your VCS SSH keys
    ```
    cat ~/.ssh/beep.pub | pbcopy
    ```

1. In your local terminal session, add your ssh private key
    ```
    ssh-add ~/.ssh/beep
    ```

1. Clone the thing!
    ```
    git clone git@gitlab.com:advin24/purple-cow-ssl-eater.git
    ```

## Build and Run locally

1. Build the local container image variant
    ```
    docker build --progress=plain -t boop-local --target local .
    ```

1.  Run the local container variant
    ```
    docker run -p 9000:8080 boop-local:latest /main
    ```

1. Hit the endpoint!
    ```
    curl -XPOST "http://localhost:9000/2015-03-31/functions/function/invocations" -d '{}'
    ```

## Build and Publish to ECR:

1. Build the image:
    ```
    docker build --progress=plain -t boop .
    ```

1. Tag the image to correspond to the ECR Registry:
    ```
    docker tag boop:latest ${AWS_ACCOUNT_ID}.dkr.ecr.${AWS_REGION}.amazonaws.com/purple-ssl-eater:latest
    ```

1. Push the image up!
    ```
    docker push ${AWS_ACCOUNT_ID}.dkr.ecr.${AWS_REGION}.amazonaws.com/purple-ssl-eater:latest
    ```

## Build out Infrastructure

There are two modules:

1. [purple-ssl-eater](/terraform/purple-ssl-eater/): Is meant for dependencies and components related to the deployment of the purple-ssl-eater service.
1. [shared](/terraform/shared/): Is meant for any required components that must exist before the purple-ssl-eater function can be created.

To run a module:

```
cd terraform/${MODULE}
terraform plan -out 'plan'
terraform apply 'plan' # if all looks good
```

The `purple-ssl-eater` module can also update the function image and API gateway deployment by passing in a variable named `image_version`

_Note_: This is helpful for local development but wanting to interact with cloud based resources.

```
terraform apply -var image_version="latest"
```

## Future Updates

Application Related:

1. Add unit tests, rewrite service with TDD
1. Implement local emulator solution
1. Remove tech debt of local Docker container, as it doesn't implement API Gateway
1. Grab certificate details directly from `apigw.go` APIGatewayCustomAuthorizerRequestTypeRequest
1. Flesh out error and debug cases in case a cert isn't returned, isn't valid, etc.

Deployment Related:

1. Implement segregated functions that correspond to **n** environments that the customer has ( Dev, Test, etc)
1. Correspond API Gateway Deployment stages to **n** environments
1. Rework `purple-ssl-eater` Terraform module to decouple API Gateway creation from function and API Gateway deployment
1. Add to the CI solution to be able to handle deploying to **n** environments
1. Implement canary deployment solution to specific environments
1. Implement testing solution to smoke test application
1. Ability to create "ephemeral" functions and API Gateway deployments

Observational Related:

1. Implement counting metrics ( Prometheus? Cloudwatch? ) to aggregate time series data.
1. Implement some form of observational graphing and alerting mechanism to capture certificate related
1. Implement more robust logging mechanism to aggregate events over time ( errors, warnings, etc )
1. Implement some form of metrics around specific performance related data.

Architecture/Infrastructure Related:

1. Determine consumers of this service, where the clients are located ( external? internal? )
1. Determine if there are any integration points with this service.
1. Potentially rework some network related components, if need be ( VPC, WAF, etc )
1. Spike on API Gateway some more, specifically with regards to v2.

## Details of Implementation

1. Currently the creation of the infrastructure and the deploying of the application is tightly coupled. Most likely this will not work in the long term.
1. Local development with docker container was set up during init development to get a golang service working. However, _it will not work_ with solution as there is no form of API Gateway locally. Ideally this could be emulated, or utilize cloud only development.
1. There is only one function out there for anyone that is attempting to use it or update it.
1. Running the CI will cause the function to and API Gateway deployment to update forcefully.
1. The service currently has no unit tests :eeek: around functional code. Future development can cause bugs easily.
1. Solution was built out with containerization in mind adding an addtional layer of complexity - if that isn't desirable.
1. Terraform seems very heavy handed for this job. I would like to spike on a easier framework ( Serverless? ) to see if it handles better.
1. There is very little visibility into the health of the function or how it is running.