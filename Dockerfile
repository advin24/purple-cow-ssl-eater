FROM public.ecr.aws/lambda/provided:al2 as build

# install compiler
RUN yum install -y golang
RUN go env -w GOPROXY=direct

# cache dependencies
ADD go.mod go.sum ./
RUN go mod download

# build
ADD . .
RUN go build -o /main

FROM public.ecr.aws/lambda/provided:al2 as local

COPY --from=build /main /main

# (Optional) Add Lambda Runtime Interface Emulator and use a script in the ENTRYPOINT for simpler local runs
ADD https://github.com/aws/aws-lambda-runtime-interface-emulator/releases/latest/download/aws-lambda-rie /usr/bin/aws-lambda-rie
RUN chmod 755 /usr/bin/aws-lambda-rie
COPY aws-lambda-emulator/entry.sh /
RUN chmod 755 /entry.sh

ENTRYPOINT [ "/entry.sh" ]

FROM public.ecr.aws/lambda/provided:al2 as test

RUN echo "this is a not a test exclamation point"

# copy artifacts to a clean image
FROM public.ecr.aws/lambda/provided:al2 as publish

COPY --from=build /main /main

ENTRYPOINT [ "/main" ]