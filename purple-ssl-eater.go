package main

import (
	"crypto/tls"
	"net/http"
	"time"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

func HandleRequest(req events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	conn, err := tls.Dial("tcp", req.RequestContext.DomainName+":443", nil)
	if err != nil {
		return events.APIGatewayProxyResponse{
			StatusCode: http.StatusNotFound,
			Body:       "Couldn't Retrieve Cert for: " + string(req.RequestContext.DomainName),
		}, nil
	}

	expiry := conn.ConnectionState().PeerCertificates[0].NotAfter

	return events.APIGatewayProxyResponse{
		StatusCode: http.StatusOK,
		Body:       "Cert Expiration: " + expiry.Format(time.RFC850),
	}, nil
}

func main() {
	lambda.Start(HandleRequest)
}
