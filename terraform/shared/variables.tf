variable "aws_region" {
  type        = string
  default     = "us-east-2"
  description = "The region associated to your AWS resources"
}

variable "name" {
  type        = string
  default     = "purple-ssl-eater"
  description = "Name of the repository"
}
