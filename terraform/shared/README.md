## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | ~> 1.0 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | 3.48.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 3.48.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_ecr_repository.application_ecr](https://registry.terraform.io/providers/hashicorp/aws/3.48.0/docs/resources/ecr_repository) | resource |
| [aws_ecr_repository_policy.application_ecr_policy](https://registry.terraform.io/providers/hashicorp/aws/3.48.0/docs/resources/ecr_repository_policy) | resource |
| [aws_iam_policy_document.application_ecr_policy_document](https://registry.terraform.io/providers/hashicorp/aws/3.48.0/docs/data-sources/iam_policy_document) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_aws_region"></a> [aws\_region](#input\_aws\_region) | The region associated to your AWS resources | `string` | `"us-east-2"` | no |
| <a name="input_name"></a> [name](#input\_name) | Name of the repository | `string` | `"purple-ssl-eater"` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_repository_url"></a> [repository\_url](#output\_repository\_url) | URL of the image repository |
