output "repository_url" {
  value       = aws_ecr_repository.application_ecr.repository_url
  description = "URL of the image repository"
}
