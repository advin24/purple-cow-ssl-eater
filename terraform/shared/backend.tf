terraform {
  backend "s3" {
    bucket         = "purple-ssl-eater-remote-state"
    dynamodb_table = "purple-ssl-eater-remote-state"
    key            = "shared/terraform.tfstate"
    encrypt        = true
    region         = "us-east-2"
  }
}
