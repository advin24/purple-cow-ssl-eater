resource "aws_ecr_repository" "application_ecr" {
  name                 = var.name
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = false
  }
}

data "aws_iam_policy_document" "application_ecr_policy_document" {
  statement {
    sid    = "LambdaECRImageRetrievalPolicy"
    effect = "Allow"
    actions = [
      "ecr:BatchGetImage",
      "ecr:GetDownloadUrlForLayer",
    ]
    principals {
      identifiers = ["lambda.amazonaws.com"]
      type        = "Service"
    }
  }
}

resource "aws_ecr_repository_policy" "application_ecr_policy" {
  repository = aws_ecr_repository.application_ecr.name
  policy     = data.aws_iam_policy_document.application_ecr_policy_document.json
}
