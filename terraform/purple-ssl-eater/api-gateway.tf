resource "aws_api_gateway_rest_api" "application_gateway" {
  name = var.name
}

resource "aws_api_gateway_resource" "application_gateway" {
  parent_id   = aws_api_gateway_rest_api.application_gateway.root_resource_id
  path_part   = "cert"
  rest_api_id = aws_api_gateway_rest_api.application_gateway.id
}

resource "aws_api_gateway_method" "application_gateway" {
  authorization = "NONE"
  http_method   = "GET"
  resource_id   = aws_api_gateway_resource.application_gateway.id
  rest_api_id   = aws_api_gateway_rest_api.application_gateway.id
}

resource "aws_api_gateway_integration" "application_gateway" {
  http_method = aws_api_gateway_method.application_gateway.http_method
  resource_id = aws_api_gateway_resource.application_gateway.id
  rest_api_id = aws_api_gateway_rest_api.application_gateway.id

  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.application_lambda.invoke_arn
}

resource "aws_api_gateway_stage" "application_gateway" {
  deployment_id = aws_api_gateway_deployment.application_gateway.id
  rest_api_id   = aws_api_gateway_rest_api.application_gateway.id
  stage_name    = "local-dev"
}

resource "aws_api_gateway_deployment" "application_gateway" {
  rest_api_id = aws_api_gateway_rest_api.application_gateway.id

  triggers = {
    redeployment = aws_lambda_function.application_lambda.image_uri
  }

  lifecycle {
    create_before_destroy = true
  }
}