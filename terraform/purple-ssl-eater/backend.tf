terraform {
  backend "s3" {
    bucket         = "purple-ssl-eater-remote-state"
    dynamodb_table = "purple-ssl-eater-remote-state"
    key            = "purple-ssl-eater/terraform.tfstate"
    encrypt        = true
    region         = "us-east-2"
  }
}

data "terraform_remote_state" "shared" {
  backend = "s3"

  config = {

    bucket         = "purple-ssl-eater-remote-state"
    dynamodb_table = "purple-ssl-eater-remote-state"
    key            = "shared/terraform.tfstate"
    region         = "us-east-2"
  }
}
