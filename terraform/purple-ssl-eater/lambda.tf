resource "aws_lambda_function" "application_lambda" {
  function_name = var.name

  package_type = "Image"

  image_uri = "${data.terraform_remote_state.shared.outputs.repository_url}:${var.image_version}"

  role = aws_iam_role.application_lambda.arn
}

resource "aws_cloudwatch_log_group" "application_lambda" {
  name = "/aws/lambda/${aws_lambda_function.application_lambda.function_name}"

  retention_in_days = 0
}

data "aws_iam_policy_document" "application_lambda" {
  statement {
    sid    = "LambdaAssumeRolePolicy"
    effect = "Allow"
    actions = [
      "sts:AssumeRole"
    ]
    principals {
      identifiers = ["lambda.amazonaws.com"]
      type        = "Service"
    }
  }
}

resource "aws_iam_role" "application_lambda" {
  name = "${var.name}-lambda-role"

  assume_role_policy = data.aws_iam_policy_document.application_lambda.json
}

resource "aws_iam_role_policy_attachment" "application_lambda" {
  role       = aws_iam_role.application_lambda.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}

resource "aws_lambda_permission" "apigw_lambda" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.application_lambda.function_name
  principal     = "apigateway.amazonaws.com"

  # More: http://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-control-access-using-iam-policies-to-invoke-api.html
  source_arn = "arn:aws:execute-api:${var.aws_region}:${data.aws_caller_identity.current.account_id}:${aws_api_gateway_rest_api.application_gateway.id}/*/${aws_api_gateway_method.application_gateway.http_method}${aws_api_gateway_resource.application_gateway.path}"
}