variable "aws_region" {
  type        = string
  default     = "us-east-2"
  description = "The region associated to your AWS resources"
}

variable "image_version" {
  type        = string
  default     = "latest"
  description = "Container Image Version to deploy in Lambda Function"
}

variable "name" {
  type        = string
  default     = "purple-ssl-eater"
  description = "Unique name to give the corresponding Function and API Gateway"
}