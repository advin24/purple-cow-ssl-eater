output "invoke_url" {
  value       = aws_api_gateway_stage.application_gateway.invoke_url
  description = "API Gateway URL"
}
