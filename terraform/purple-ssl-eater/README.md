## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | ~> 1.0 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | 3.48.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 3.48.0 |
| <a name="provider_terraform"></a> [terraform](#provider\_terraform) | n/a |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_api_gateway_deployment.application_gateway](https://registry.terraform.io/providers/hashicorp/aws/3.48.0/docs/resources/api_gateway_deployment) | resource |
| [aws_api_gateway_integration.application_gateway](https://registry.terraform.io/providers/hashicorp/aws/3.48.0/docs/resources/api_gateway_integration) | resource |
| [aws_api_gateway_method.application_gateway](https://registry.terraform.io/providers/hashicorp/aws/3.48.0/docs/resources/api_gateway_method) | resource |
| [aws_api_gateway_resource.application_gateway](https://registry.terraform.io/providers/hashicorp/aws/3.48.0/docs/resources/api_gateway_resource) | resource |
| [aws_api_gateway_rest_api.application_gateway](https://registry.terraform.io/providers/hashicorp/aws/3.48.0/docs/resources/api_gateway_rest_api) | resource |
| [aws_api_gateway_stage.application_gateway](https://registry.terraform.io/providers/hashicorp/aws/3.48.0/docs/resources/api_gateway_stage) | resource |
| [aws_cloudwatch_log_group.application_lambda](https://registry.terraform.io/providers/hashicorp/aws/3.48.0/docs/resources/cloudwatch_log_group) | resource |
| [aws_iam_role.application_lambda](https://registry.terraform.io/providers/hashicorp/aws/3.48.0/docs/resources/iam_role) | resource |
| [aws_iam_role_policy_attachment.application_lambda](https://registry.terraform.io/providers/hashicorp/aws/3.48.0/docs/resources/iam_role_policy_attachment) | resource |
| [aws_lambda_function.application_lambda](https://registry.terraform.io/providers/hashicorp/aws/3.48.0/docs/resources/lambda_function) | resource |
| [aws_lambda_permission.apigw_lambda](https://registry.terraform.io/providers/hashicorp/aws/3.48.0/docs/resources/lambda_permission) | resource |
| [aws_caller_identity.current](https://registry.terraform.io/providers/hashicorp/aws/3.48.0/docs/data-sources/caller_identity) | data source |
| [aws_iam_policy_document.application_lambda](https://registry.terraform.io/providers/hashicorp/aws/3.48.0/docs/data-sources/iam_policy_document) | data source |
| [terraform_remote_state.shared](https://registry.terraform.io/providers/hashicorp/terraform/latest/docs/data-sources/remote_state) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_aws_region"></a> [aws\_region](#input\_aws\_region) | The region associated to your AWS resources | `string` | `"us-east-2"` | no |
| <a name="input_image_version"></a> [image\_version](#input\_image\_version) | Container Image Version to deploy in Lambda Function | `string` | `"latest"` | no |
| <a name="input_name"></a> [name](#input\_name) | Unique name to give the corresponding Function and API Gateway | `string` | `"purple-ssl-eater"` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_invoke_url"></a> [invoke\_url](#output\_invoke\_url) | API Gateway URL |
